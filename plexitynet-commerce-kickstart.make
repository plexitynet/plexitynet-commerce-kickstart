; Drush make file for PlexityNet Commerce Kickstart 7.x-2.x multisites.
api = "2"


; Core
; ----
core = 7.x

projects[commerce_kickstart][type] = "core"
projects[commerce_kickstart][download][type] = "file"
projects[commerce_kickstart][download][url] = "http://ftp.drupal.org/files/projects/commerce_kickstart-7.x-2.55-core.tar.gz"


; Contrib modules
; ---------------

; Contrib modules for PlexityNet.

projects[captcha][type] = "module"
projects[captcha][subdir] = "contrib"
projects[captcha][version] = "1.5"

projects[commerce_bank_transfer][type] = "module"
projects[commerce_bank_transfer][subdir] = "contrib"
projects[commerce_bank_transfer][version] = "1.0-alpha3"

projects[commerce_google_analytics][type] = "module"
projects[commerce_google_analytics][subdir] = "contrib"
projects[commerce_google_analytics][version] = "2.0"

projects[css_injector][type] = "module"
projects[css_injector][subdir] = "contrib"
projects[css_injector][version] = "1.10"

projects[ga_push][type] = "module"
projects[ga_push][subdir] = "contrib"
projects[ga_push][version] = "1.1"

projects[google_analytics][type] = "module"
projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "2.5"

projects[persistent_login][type] = "module"
projects[persistent_login][subdir] = "contrib"
projects[persistent_login][version] = "1.1"

projects[recaptcha][type] = "module"
projects[recaptcha][subdir] = "contrib"
projects[recaptcha][version] = "2.2"

projects[webform][type] = "module"
projects[webform][subdir] = "contrib"
projects[webform][version] = "4.18"


; Custom modules
; --------------

projects[pn_branding][type] = "module"
projects[pn_branding][subdir] = "custom"
projects[pn_branding][download][type] = "git"
projects[pn_branding][download][url] = "git@bitbucket.org:plexitynet/pn_branding.git"
projects[pn_branding][download][branch] = "7.x"

